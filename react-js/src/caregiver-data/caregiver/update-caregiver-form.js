import React from 'react';
import validate from "./validators/caregiver-validators";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import CaregiverTextInput from "./fields/CaregiverTextInput";

class UpdateCaregiverForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                name: {
                    value: '',
                    placeholder: 'Full Name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    handleUpdate = ( ) => {
        let newCaregiverDate = {
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
        };
        console.log("caregiveer ", newCaregiverDate);
        this.props.update(newCaregiverDate);  //call a props method
    }

    render() {
        return (
            <div>
                <form>

                    <h1>Update data for this caregiver:</h1>

                    <p> Name: </p>
                    <CaregiverTextInput name="name"
                                      placeholder={this.state.formControls.name.placeholder}
                                      value={this.state.formControls.name.value}
                                      onChange={this.handleChange}
                                      touched={this.state.formControls.name.touched}
                                      valid={this.state.formControls.name.valid}
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                    <p> Address: </p>
                    <CaregiverTextInput name="address"
                                      placeholder={this.state.formControls.address.placeholder}
                                      value={this.state.formControls.address.value}
                                      onChange={this.handleChange}
                                      touched={this.state.formControls.address.touched}
                                      valid={this.state.formControls.address.valid}
                    />

                    <p></p>

                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                </form>

                <Button onClick= {() => {
                    this.handleUpdate()
                }}
                        disabled={!this.state.formIsValid}>
                    Save the changes
                </Button>
            </div>


        );
    }
}

export default UpdateCaregiverForm;