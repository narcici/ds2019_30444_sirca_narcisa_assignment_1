import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import Users from './user/user'
import Patients from './patient-data/patient/patients'
import Caregivers from './caregiver-data/caregiver/caregivers'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Medications from "./medication-data/medication/medications";
import Login from "./roles/login";
import DoctorRole from "./roles/doctorRole";
import PatientRole from "./roles/patientRole";
import CaregiverRole from "./roles/caregiverRole";

let enums = require('./commons/constants/enums');

class App extends React.Component {
    componentDidMount() {
        localStorage.setItem("lastInsertedUserId", "0");
        localStorage.setItem("isPatientUserInserted", "false");
    }

    render() {

        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <NavigationBar/>
                        <Switch>

                            <Route
                                exact
                                path='/'
                                render={() => <Login />}
                            />

                            <Route
                                exact
                                path='/home'
                                render={() => <Home />}
                            />

                            <Route
                                exact
                                path='/persons'
                                render={() => <Persons />}
                            />

                            <Route
                                exact
                                path='/user'
                                render={() => <Users />}
                            />

                            <Route
                                exact
                                path='/patient'
                                render={() => <Patients />}
                            />

                            <Route
                                exact
                                path='/caregiver'
                                render={() => <Caregivers />}
                            />
                            <Route
                                exact
                                path='/medication'
                                render={() => <Medications />}
                            />

                            <Route
                                exact
                                path='/doc'
                                render={() => <DoctorRole />}
                            />

                            <Route
                                exact
                                path='/pat'
                                render={() => <PatientRole />}
                            />

                            <Route
                                exact
                                path='/care'
                                render={() => <CaregiverRole />}
                            />

                            Error
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
