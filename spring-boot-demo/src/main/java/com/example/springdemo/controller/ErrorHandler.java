package com.example.springdemo.controller;

import com.example.springdemo.dto.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Component
@RestControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST) //400 at response
    @ExceptionHandler(NullPointerException.class)
    public ErrorDTO handlerNPE(NullPointerException ex) {
        return new ErrorDTO("NPE");
    }
}
