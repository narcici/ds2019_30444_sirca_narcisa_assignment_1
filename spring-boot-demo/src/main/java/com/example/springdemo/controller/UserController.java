package com.example.springdemo.controller;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/username/{u}")
    public UserDTO findByUsername(@PathVariable("u") String username){
        return userService.findByUsername(username);
    }

    @GetMapping()   //un ex de get
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody UserDTO userDTO) {
        return userService.update(userDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody UserDTO userViewDTO){
        userService.delete(userViewDTO);
    }
}
