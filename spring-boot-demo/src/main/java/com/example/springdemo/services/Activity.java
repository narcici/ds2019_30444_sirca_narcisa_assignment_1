package com.example.springdemo.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public final class Activity implements Serializable {

    private final int id;
    private final String activity;
    private final Date start;
    private final Date end;

    public Activity(@JsonProperty("id") int id,
                    @JsonProperty("activity") String activity,
                    @JsonProperty("start") Date start,
                    @JsonProperty("end") Date end
    ) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + "'" +
                ", activity=" + activity +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}