package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.DoctorWithPatientsDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.dto.builders.DoctorWithPatientsBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public List<DoctorDTO> findAllDoctors(){
            List<Doctor> doctors = doctorRepository.findAll();
            return doctors.stream()
                    .map(DoctorBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());
    }

    public DoctorWithPatientsDTO findDoctorById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        return DoctorWithPatientsBuilder.generateDTOFromEntity(doctor.get(), doctor.get().getPatients());
    }

    public List<DoctorWithPatientsDTO> findAllFetch(){
        List<Doctor> personList = doctorRepository.getAllFetch();

        return personList.stream()
                .map(x-> DoctorWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }

    public DoctorWithPatientsDTO findDoctorByUserid(Integer userid){
        List<DoctorWithPatientsDTO> allDoctors = findAllFetch();
        List<DoctorWithPatientsDTO> result = allDoctors.stream()
                .filter(c -> c.getUser().getId() == userid)
                .collect(Collectors.toList());
        DoctorWithPatientsDTO caregiver = result.get(0);

        return caregiver;
    }
}
