package com.example.springdemo.dto;

import lombok.Data;

@Data
public class ErrorDTO {
    private final String type;
}
