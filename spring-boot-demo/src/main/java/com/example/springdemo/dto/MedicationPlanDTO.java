package com.example.springdemo.dto;

import com.example.springdemo.entities.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicationPlanDTO {

    private Integer medicationPlanId;
    private Date startDate;
    private Date endDate;
    //private Patient patient;


}
