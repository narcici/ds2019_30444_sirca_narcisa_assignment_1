package com.example.springdemo.dto;

import com.example.springdemo.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverDTO {

    private User user; //inherited userid
    private Integer caregiverId;
    private String name;
    private  String birthdate;
    private String address;
    private Character gender;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(user, caregiverDTO.user) &&
                Objects.equals(caregiverId, caregiverDTO.caregiverId) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(birthdate, caregiverDTO.birthdate) &&
                Objects.equals(address, caregiverDTO.address) &&
                Objects.equals(gender, caregiverDTO.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, caregiverId, name, address, gender, birthdate);
    }

}
