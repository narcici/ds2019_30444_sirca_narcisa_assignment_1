package com.example.springdemo.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medication_planId", unique = true, nullable = false)
    private Integer medicationPlanId;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @ManyToMany(mappedBy = "medicationPlan", fetch = FetchType.LAZY)
    private List<IntakeInterval> intakeIntervalsList = new ArrayList<IntakeInterval>();

    public MedicationPlan(Integer medicationPlanId, Date startDate, Date endDate, Patient patient) {
        this.medicationPlanId = medicationPlanId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
    }

    public MedicationPlan(Integer medicationPlanId, Date startDate, Date endDate) {
        this.medicationPlanId = medicationPlanId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}