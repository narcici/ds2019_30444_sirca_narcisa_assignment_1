package com.example.springdemo.repositories;

import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.username = ?1"
    )
    User findByUsername(String username); ///param passing
}
